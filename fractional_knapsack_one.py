# Uses python3
import sys

# Problem: Fractional Knapsack
#
# This is my naive solution for the fractional knapsack problem.async
# It passes majority of the test cases; however, it fails on one of them,
# returning a non expected result. Unfortunately the automatic grader
# does not give me anymore information then that so its probably best
# that I start again from scratch and implement this in a different fashion.

def get_optimal_value(capacity, weights, values):
    value = 0.
    knapsack = []
    if capacity == 0:
        return value
    elif len(values) <= 1 and values[0] < capacity:
        value += values[0]
    else:
        while capacity > 0:
            sorted_pairs = sorted(zip(values, weights))
            max_sorted = max(sorted_pairs)
            min_sorted = min(sorted_pairs)
            if max_sorted[1] > capacity:
                if min_sorted[1] <= capacity:
                    knapsack.append(min_sorted[0])
                    capacity -= min_sorted[1]
                else:
                    x = max_sorted[1] / capacity
                    value +=  max_sorted[0] / x
                    capacity -= max_sorted[1]
            else:
                knapsack.append(max_sorted[0])
                capacity -= max_sorted[1]
                weights.remove(max_sorted[1])
                values.remove(max_sorted[0])
        value += sum(knapsack)
    return abs(value)


if __name__ == "__main__":
    data = list(map(int, sys.stdin.read().split()))
    n, capacity = data[0:2]
    values = data[2:(2 * n + 2):2]
    weights = data[3:(2 * n + 2):2]
    opt_value = get_optimal_value(capacity, weights, values)
    print("{:.10f}".format(opt_value))
