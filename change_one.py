
# Changing Money
# Task. The goal in this problem is to find the minimum number of coins needed to change the given amount
# of money using coins with denominations 1, 5, and 10.

# This was my naive solution, which did not pass the running time and memory restrictions of the unit test grader.
# I was creating 3 variables in memory to represent each of the coins, and a variable x to
# count each time a coin was used, which looking back is a lot of back and forth. On top of that, I used recursion
# to decrement the variable n while incrementing the x. This solution solved the problem; however
# I greatly improved the run time and memory usage in my new solution.


def get_change(n):
    coin_a = 1
    coin_b = 5
    coin_c = 10
    x = 0
    while n > 0:
        if coin_c <= n:
            x = x + 1
            n = n - coin_c
            get_change(n)
        elif coin_b <= n:
            x = x + 1
            n = n - coin_b
            get_change(n)
        elif coin_a <= n:
            x = x + 1
            n = n - coin_a
            get_change(n)
    return int(x)

