# Uses python3
import sys

# Problem: Fractional Knapsack
# Second attempt, This one should be slightly more accurate; but isn't passing
# because of performance. Doesn't surprise me since im iterating all over
# the fucking place.

def get_optimal_value(capacity, weights, values):
    value = 0.
    knapsack = []
    filtered_weights = []
    filtered_values = []
    if capacity == 0:
        return value
    if len(values) > 1:
        while capacity > 0:
            for val, weight in zip(values, weights):
                if weight <= capacity:
                    filtered_weights.append(weight)
                    filtered_values.append(val)
            for val, weight in zip(filtered_values, filtered_weights):
                if val == max(filtered_values):
                    knapsack.append(val)
                    values.remove(val)
                    weights.remove(weight)
                    filtered_weights.clear()
                    filtered_values.clear()
                    capacity -= weight
    else:
        for val, weight in zip(values, weights):
            if weight > capacity:
                d = weight / capacity
                value += val / d
            else:
                value += values[0]

    value += sum(knapsack)
    return abs(value)


if __name__ == "__main__":
    data = list(map(int, sys.stdin.read().split()))
    n, capacity = data[0:2]
    values = data[2:(2 * n + 2):2]
    weights = data[3:(2 * n + 2):2]
    opt_value = get_optimal_value(capacity, weights, values)
    print("{:.10f}".format(opt_value))
