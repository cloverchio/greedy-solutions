# Uses python3
import sys

# Changing Money
# Task. The goal in this problem is to find the minimum number of coins needed to change the given amount
# of money using coins with denominations 1, 5, and 10.


def get_change(n):
    coins = [1, 5, 10]
    coins_used = []
    while n > 0:
        if n >= coins[2]:
            coins_used.append(coins[2])
            n -= coins[2]
        elif n >= coins[1]:
            coins_used.append(coins[1])
            n -= coins[1]
        elif n >= coins[0]:
            coins_used.append(coins[0])
            n -= coins[0]
    n = len(coins_used)
    return n


if __name__ == '__main__':
    n = int(sys.stdin.read())
    print(get_change(n))
