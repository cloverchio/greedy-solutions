import java.util.*;

/**
 * Lets try this again...
 * <p>
 * Problem: Fractional Knapsack (0/1 Knapsack)
 * <p>
 * CAPTAIN'S LOGS - DAY 10012013810:
 * <p>
 * Looks like the first thing I'll do is create an array of size N which should effectively be
 * the length of the values array (specified in main method).
 * <p>
 * Second I'll loop from 1 to N - Checking to see if the value divided by weight is greater than the previous
 * value divided by weight.
 * <p>
 * In this case I only care about the biggest values / weights so I'm hoping Math.max() will
 * take care of that for me...
 * <p>
 * If the value at the specified index divided by weight is equal to the quotient provided from before
 * I know that I care about it and should use that value...
 * Plus some other non base case logic.
 * <p>
 * <p>
 * <p>
 *
 * Failed case #6/13: Wrong answer
 * got: 46472.0 expected: 7777.731
 * (Time used: 0.25/1.50, memory used: 22016000/671088640.)
 */

public class FractionalKnapsackThree {

  private static double getOptimalValue(int capacity, int[] values, int[] weights) {

    double value = 0;
    int n = values.length;
    int[] maxQuotients = new int[n];
    if (n > 1) {
      for (int i = 1; i < n; i++) {
        if (capacity == 0) {
          return value;
        }
        int maxDivisor = Math.max(values[i] / weights[i], values[i - 1] / weights[i - 1]);
        maxQuotients[i] = maxDivisor;
      }
      for (int d : maxQuotients) {
        for (int i = 0; i < n; i++) {
          if (values[i] / weights[i] == d) {
            value += (double) values[i];
          }
        }
      }
    }
    if (n == 1) {
      if (weights[0] > capacity) {
        value += (double) values[0] / (weights[0] / capacity);
      }
      else {
        value += (double) values[0];
      }
    }

    return Math.round(value * 1000) / 1000.00;
  }

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    int capacity = scanner.nextInt();
    int[] values = new int[n];
    int[] weights = new int[n];
    for (int i = 0; i < n; i++) {
      values[i] = scanner.nextInt();
      weights[i] = scanner.nextInt();
    }
    System.out.println(getOptimalValue(capacity, values, weights));


  }
}
