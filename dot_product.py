#Uses python3

import sys

# Problem: Minimum Dot Product
#
# Looks like for this one the goal is given two sequences, to multiply
# the max numbers in one sequence to the min numbers in the other to get the
# minimum dot product.

def min_dot_product(a, b):
    res = 0
    a_desc = sorted(a, reverse=True)
    b_asc = sorted(b)
    for i in range(len(a)):
        res += a_desc[i] * b_asc[i]
    return res

if __name__ == '__main__':
    input = sys.stdin.read()
    data = list(map(int, input.split()))
    n = data[0]
    a = data[1:(n + 1)]
    b = data[(n + 1):]
    print(min_dot_product(a, b))
    
